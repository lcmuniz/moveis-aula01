# Desenvolvimento de Aplicativos para Dispositivos Móveis

Aula 02 - Android Sudio

Prof. Luiz Carlos Melo Muniz

lcmuniz@gmail.com

---

### Estrutura de uma aplicação Android

![Estrutura de uma aplicação Android](text3750-9-8-6.png)


---

### Componentes de uma aplicação

![Componentes de uma aplicação](text3750-9-8-5.png)

